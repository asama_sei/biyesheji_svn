@projectInfo
/***模块编号对应关系
        base         ---> U1
***/
/***模块函数列表
        base        
>>  void delay_10_us( );
>>  void delay_ms(unsigned char _U1_time);
***/
@start
#include "reg52.h"
#include "intrins.h"
@define

@glo
//请在此写入全局变量

@code
//请在此编写函数

@main
//仅执行一次的代码
void setup()
{

}
//重复执行的代码
void loop()
{
	delay_ms(1000);
	P0=0x00;
	delay_ms(1000);
	P0=0xff;
}
@end
//请不要在此编写任何东西
