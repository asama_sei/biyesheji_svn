# coding: utf-8
require 'gtk3'
require 'to_bool'
require 'aes'


def banner_package
   AES.decrypt File.open("Version",'r').read,""
end
$versionString = 
"
YOU ARE IN Ver
   0001.1
"+banner_package

# Core+
Gtk::TreeView.class_eval do
   def clear
      self.model.clear
   end
end
class Array
   alias_method :base_count,:count
   def path
      self[0]
   end
   def base
      self[1]
   end
   def fun
      self[2]
   end
   def setting
      self[3]
   end
   def sim
      self[4]
   end
   def count
      self[5]
   end
   def dbg
      self[6]
   end
end

class FileReader < File
   attr_accessor :configs
   def get_config(countNo)
   #TODO
   #IMPORTANT
   #删除空项
      @configs = Hash.new
      baseRoom = String.new
      self.each_line do |line|
         line.chomp!
         line.strip!
         if line.start_with? "@"
            baseRoom = line.sub /[@]/,""
            @configs[baseRoom] = []
         else
            @configs[baseRoom].push line.gsub(/#/,"_"+countNo+"_").tr("`","#") if line != nil
         end
      end
      @configs
   end
   def get_code
      #TODO
      #IMPORTANT
      #删除空项
      @configs = Hash.new
      baseRoom = String.new "outArea"
      self.each_line do |line|
         line.chomp!
         line.strip!
         if line.start_with? "@"
            baseRoom = line.sub /[@]/,""
            @configs[baseRoom] = []
         else
            @configs[baseRoom].push line if line != nil
         end
      end
      @configs
   end
   def self.band_sub_setting(*args)
      args.each do |arg|
         define_method ('get_'+arg).to_sym do
            @configs[arg]
         end
      end
   end
end

class ModBase < FileReader
   band_sub_setting 'include','define','map','code'

   def init countNo
      self.get_config countNo
      self.map_init
   end

   def map_init
      if @configs.key? "map"
         @configs['map'].collect! do |map|
            map.split ","
         end
      end
   end
end
class ModFun < FileReader
   band_sub_setting 'base','extra','funName'
   def init countNo
      self.get_config countNo
      self.extra_init if @configs.has_key? 'extra'
      self.funName_init

      @configs
   end

   def extra_init
      newExtra = Hash.new
      nameRoom = ""
      @configs['extra'].each do |line|
         if line.start_with? '^'
            line = line.split '^'
            line.delete_at(0)
            nameRoom = line.delete_at(0)
            newExtra[nameRoom] = [line,[]]
         else
            newExtra[nameRoom][1].push line
         end
      end
      @configs['extra'] = newExtra
   end
   def funName_init
      @configs['funName'] = Array.new
      @configs['base'].collect! do |subLn|
         if subLn.start_with? "^"
            subLn.delete!("^") 
            @configs['funName'].push subLn.tr("{","")+';'
         end
         subLn
      end
      if @configs.has_key? 'extra'
         @configs['extra'].each_value do |subFun|
            @configs['funName'].push subFun[1][0].tr("{","")+";"
         end
      end
   end
end
class ModSetting < File

end
class ModDbg < FileReader
   band_sub_setting "dbg"
   def init countNo
      self.get_config countNo
      self.dbg_init
      @configs
   end
   def dbg_init
      if @configs['dbg'].class != nil.class
         @configs['dbg'].collect! do |subLn|
            if subLn.start_with? "^"
               subLn.delete!("^") 
            end
            subLn
         end
      else
         @configs['dbg'] = Array.new
      end
   end
end
class ModSim < FileReader
   band_sub_setting "sim"
   def init countNo
      self.get_config countNo
      self.sim_init
      @configs
   end
   def sim_init
      if @configs['sim'].class != nil.class
         @configs['sim'].collect! do |subLn|
            if subLn.start_with? "^"
               subLn.delete!("^") 
            end
            subLn
         end
      else
         @configs['sim'] = Array.new
      end
   end
end
class FileCode < FileReader
   def get_code()
      #TODO
      #IMPORTANT
      #删除空项
      @configs = Hash.new
      baseRoom = String.new "outArea"
      self.each_line do |line|
         line.chomp!
         line.strip!
         if line.start_with? "@"

            if line.start_with? "@::"
               #TODO
               case line
               when "@::INT1"
                  @configs[baseRoom].push "void __PROJECT_INT_OUT0() interrupt 0"
               when "@::INT2"
                  @configs[baseRoom].push "void __PROJECT_INT_OUT1() interrupt 2"
               when "@::INTT1"
                  @configs[baseRoom].push "void __PROJECT_INT_TIME0() interrupt 1"
               when "@::INTT2"
                  @configs[baseRoom].push "void __PROJECT_INT_TIME1() interrupt 3"
               when "@::INTIO"
                  @configs[baseRoom].push "void __PROJECT_INT_IO() interrupt 4"
               else
                  @configs[baseRoom].push "void #{line.tr "@:","__"}()"
               end
            else
               baseRoom = line.sub /[@]/,""
               @configs[baseRoom] = []
            end
         else
            @configs[baseRoom].push line if line != nil
         end
         @configs
      end
   end
end
class FileCodeConfig < FileReader
end
class FileCodeSim < FileReader
end

class Mod < Array
   attr_accessor :configs
   def initialize(*args)
      super(args)
      @@modCount=0
      @configs = Hash.new
   end
   def add(modPath)
      @@modCount = @@modCount+1
      self.push [modPath,ModBase.new(modPath+"\\base.data"),ModFun.new(modPath+"\\fun.data"),ModSetting.new(modPath+"\\setting.data"),ModSim.new(modPath+"\\sim.data"),to_count_num(@@modCount),ModDbg.new(modPath+"\\dbg.data")] if check(modPath)

      check(modPath)
   end
   def do_init
      self.each do |subMod|
         yield("正在读取"+subMod.path+"做为"+subMod.count)
         subMod.base.init subMod.count
         subMod.fun.init subMod.count
         subMod.sim.init subMod.count
         subMod.dbg.init subMod.count
      end
   end
   def do_range
      self.each do |subMod|
         subMod.base.configs.each do |key,value|
            @configs[key] = @configs[key].to_a+value
         end
         subMod.fun.configs.each do |key,value|
            @configs[key] = @configs[key].to_a+value unless key == 'extra'
         end
         subMod.sim.configs.each do |key,value|
            @configs[key] = @configs[key].to_a+value
         end
         subMod.dbg.configs.each do |key,value|
            @configs[key] = @configs[key].to_a+value
         end
      end
      @configs
   end
   def do_uniq
      @configs['include'].uniq!
      @configs['map'].uniq!

      yield('Mod间出现 预定义 冲突') if @configs['define'].uniq != @configs['define']
      yield('Mod间出现 函数定义 冲突') if @configs['funName'].uniq != @configs['funName']
      @configs
   end
   def check(modPath)
      File.exist?(modPath+"\\base.data")&&File.exist?(modPath+"\\fun.data")&&File.exist?(modPath+"\\setting.data")&&File.exist?(modPath+"\\sim.data")&&File.exist?(modPath+"\\dbg.data")
   end
   def to_count_num(n)
      "U"+n.to_s
   end
   def self.getName(modPath)
      File.split(modPath)[-1]
   end
end

class CodeDir < Dir  
   def initialize(filename) #读取目录
      super File.split(filename)[0]                                           #工程目录
      @codeFile = File.new filename,'a+:utf-8'       #代码文件
      @codeConfig = File.new self.path+"\\config.Data",'a+:utf-8'
      @simConfig = File.new self.path+"\\sim.Data",'a+:utf-8'
      @dbgConfig = File.new self.path+"\\dbg.Data",'a+:utf-8'
      @modDir = Dir.new Dir.pwd.gsub(/./){|s| s='\\' if s == '/';s} + '\mods' #软件mod目录
      @codeMod = self.path + '\mods'                                          #代码mod目录
      if Dir.exist?(@codeMod)
         @codeMod = Dir.new @codeMod
      else
         @codeMod = nil
      end
      
   end
   def do_write_note(mods)
      yield("正在确立模块关系")
      @codeFile.write "@projectInfo\n/***模块编号对应关系\n"
      mods.each do |subMod|
         @codeFile.write "#{Mod.getName(subMod.path).center(20)} ---> #{subMod.count}\n"
      end
      @codeFile.write "***/\n/***模块函数列表\n"
      mods.each do |subMod|
         @codeFile.write "#{Mod.getName(subMod.path).center(20)}\n"
         subMod.fun.configs['funName'].each do |ln|
            @codeFile.write ">>  #{ln}\n"
         end
      end
      @codeFile.write "***/\n"
   end
   def do_write(configs) # define=>define funname=>glo code=>code
      yield "正在写入设置"
      @codeConfig.write("@map\n")
      configs['map'].each do |subMap|
         @codeConfig.write "#define #{subMap[0]} #{subMap[1]}\n"
      end
      @codeConfig.write("@funname\n")
      configs['funName'].each do |subFn|
         @codeConfig.write subFn+"\n"
      end
      @codeConfig.write "@fun\n"
      configs['base'].each do |codeLn|
         @codeConfig.write codeLn+"\n"
      end
      yield "正在准备主程序"
      @codeFile.write "@start\n"
      configs['include'].each do |includeLn|
         @codeFile.write "#include \"#{includeLn}\"\n"
      end
      @codeFile.write "@define\n"
      configs['define'].each do |defineLn|
         @codeFile.write "#define #{defineLn} P0 //TODO:在此修改为实际参数 \n"
      end

      configs['code'].each do |codeLn|
         @codeFile.write codeLn+"\n"
      end
      yield "正在准备仿真"
      @simConfig.write "@sim\n"
      configs['sim'].each do |simLn|
         @simConfig.write simLn+"\n"
      end
      yield "正在准备调试"
      @dbgConfig.write "@dbg\n"
      configs['dbg'].each do |simLn|
         @dbgConfig.write simLn+"\n"
      end
   end
   def do_write_userCode
      @codeFile.write <<~SETUP_FUN
         @glo
         //请在此写入全局变量

         @code
         //请在此编写函数
         
         @main
         //仅执行一次的代码
         void setup()
         {

         }
         //重复执行的代码
         void loop()
         {

         }
         @end
         //请不要在此编写任何东西
      SETUP_FUN
      
      
   end
   def close
      super
      @codeFile.close 
      @codeConfig.close
      @modDir.close 
      @simConfig.close
      @dbgConfig.close
      @codeMod.close if @codeMod.class == Dir
   end

   def modList #获取 MOD 列表
      listArr = []
      Dir.foreach(@modDir.path) do |subMods| #软件mod列表
         listArr.push @modDir.path + "\\" + subMods unless (subMods.casecmp '..')==0 || (subMods.casecmp '.')==0
      end
      if @codeMod.class == Dir               #代码mod列表
         Dir.foreach(@codeMod.path) do |subMods|
            listArr.push @codeMod.path + "\\" + subMods unless (subMods.casecmp '..')==0 || (subMods.casecmp '.')==0
         end
      end 
      listArr.delete_if do |subMods|
         !(File.directory? subMods)
      end
      listArr
   end
   def clear_code
      @codeFile.close
      @codeFile = FileCode.new self.path+'\\main.c','w:utf-8' 
   end
   def clear_config
      @codeConfig.close
      @codeConfig = FileCodeConfig.new self.path+"\\config.Data",'w:utf-8'
   end
   def clear_sim
      @simConfig.close
      @simConfig = File.new self.path+"\\sim.Data",'w:utf-8'
   end
   def clear_dbg
      @dbgConfig.close
      @dbgConfig = File.new self.path+"\\dbg.Data",'w:utf-8'
   end
end
class ProjectDir < Dir
   def initialize(pathProject)
      super pathProject
      @configs=Hash.new
      @codeFile = FileCode.new pathProject+"\\main.c","r:utf-8"
      @configFile = FileCodeConfig.new pathProject+"\\config.Data","r:utf-8"
      @simFile = FileCodeSim.new pathProject+"\\sim.Data","r:utf-8"
      @dbgFile = FileCodeSim.new pathProject+"\\dbg.Data","r:utf-8"
      @outputFile = File.new pathProject+"\\output.c","w"
   end

   def do_init
      @codeFile.get_code
      @configFile.get_code
      @simFile.get_code
      @dbgFile.get_code
      @codeFile.configs.each do |key,value|
         @configs[key]=value
      end
      @configFile.configs.each do |key,value|
         @configs[key]=value
      end
      @simFile.configs.each do |key,value|
         @configs[key]=value
      end
      @dbgFile.configs.each do |key,value|
         @configs[key]=value
      end
      @configs
   end
   def do_write_base(*cfg)
      cfg.each do |scfg|
         @configs[scfg].each do |lnLn|
            @outputFile.write lnLn+"\n"
         end
      end
   end
   def create_uv4_project
      uvProj = File.new self.path+"\\main.uvproj","w"
      File.new(".\\mods\\1","r").each_line do |sbL|
         uvProj.write sbL
      end
      starUp = File.new self.path+"\\STARTUP.A51","w"
      File.new(".\\mods\\2","r").each_line do |sbL|
         starUp.write sbL
      end
      starUpOpt = File.new self.path+"\\main.uvopt","w"
      File.new(".\\mods\\3","r").each_line do |sbL|
         starUpOpt.write sbL
      end
      uvProj.close
      starUp.close
      starUpOpt.close
   end

   def do_output
      @outputFile.write <<~OUTPUTFUN
         #include "stdio.h"
      OUTPUTFUN
      do_write_base 'start','map','define','glo','funname','code','main','fun'
      @outputFile.write <<~OUTPUTFUN
         int main(){
            setup();
            while(1) loop();
            return 0;
         }
      OUTPUTFUN
   end
   def do_sim
      do_write_base 'start','map','define','glo','funname','code','main','sim'
      @outputFile.write <<~OUTPUTFUN
         int main(){
            char sim_time_conts = 10;
            setup();
            while(sim_time_conts--)
            {
               puts("初始化部分完成，开始循环");
               printf("还剩 %d 趟",sim_time_conts);
               loop();
            }
            puts("程序结束");
            return 0;
         }
      OUTPUTFUN
   end
   def do_dbg
      do_write_base 'start','map','define','glo','funname','code','main','dbg'
      @outputFile.write <<~OUTPUTFUN
         int main(){
            setup();
            while(1) loop();
            return 0;
         }
      OUTPUTFUN
   end

   def close
      @codeFile.close
      @configFile.close
      @simFile.close
      @dbgFile.close
      @outputFile.close
   end
end
class GuiBuilder < Gtk::Builder
   def initialize(filename)
      super()
      #读取界面文件
      add_from_file filename

      #绑定退出按钮
      self['mainWindow'].signal_connect('destroy') { Gtk.main_quit }
      
      #添加banner
      add_banner

      #绑定并渲染TreeView
      self['createTreeView'].model = Gtk::ListStore.new(TrueClass,String,String)
      columns 'createTreeView'

      #初始化buffer
      bufferChange 'createTextView',"请设置工程目录位置并点击 Select \n"

      #设置标题
      self['mainWindow'].title='单片机辅助C程序设计'

      #锁定功能
      self['pretDebug'].sensitive=false
      self['testHelperPath'].sensitive=false
      #self['testButton'].sensitive=false

      #读取配置
      softConfig = FileReader.new "setting"
      softConfig.get_code
      configUV = softConfig.configs['uv'][0]
      configGCC = softConfig.configs['gcc'][0]
      softConfig.close
      self['outputToolPath'].filename = configUV
      self['gccPath'].filename = configGCC

      #加载窗口
      self['loadingFrame'].add( Gtk::Image.new(:animation=>GdkPixbuf::PixbufAnimation.new("loading.gif")))

      #绑定按钮
      self['buttonCreate'].signal_connect("clicked"){ # 创建 按钮
         if self['createFileChooser'].filename.class == String
            bufferAddLnRolling 'createTextView',"目录为:#{self['createFileChooser'].filename} \n 创建main.c 成功"
            codeDir = CodeDir.new self['createFileChooser'].filename+'\main.c' 
            bufferAddLnRolling 'createTextView','正在扫描mod'
            self['createTreeView'].clear
            codeDir.modList.each do |subMod|
               add_view_line 'createTreeView',[Mod.getName(subMod),subMod]
            end
            codeDir.close
            
            bufferAddLnRolling 'createTextView','请选择需要使用的mod并点击 Create 按钮'
            self['buttonCreateFinish'].sensitive=true
            self['filechooserPret'].filename=self['createFileChooser'].filename
            self['outputProjectFileSelect'].filename=self['createFileChooser'].filename
         else
            bufferAddLnRolling 'createTextView','无法找到选择的文件或文件夹'
         end
      }
      self['buttonCreateFinish'].signal_connect("clicked"){ # 创建确定 按钮
         mods = Mod.new

         get_select('createTreeView').each do |iter|
            bufferAddLnRolling 'createTextView',"正在添加#{iter[0]}"
            if mods.add iter[1]
               bufferAddLnRolling 'createTextView',"成功"
            else
               bufferAddLnRolling 'createTextView',"失败#{iter[0]}数据不完整"
            end
         end
         unless mods.empty?
            bufferAddLnRolling 'createTextView',"初始化中"
            mods.do_init{ |msg| bufferAddLnRolling 'createTextView',msg }
            mods.do_range
            mods.do_uniq{ |msg| bufferAddLnRolling 'createTextView',msg }
            bufferAddLnRolling 'createTextView',"初始化结束"
            bufferAddLnRolling 'createTextView',"清理代码"
            codeDir = CodeDir.new self['createFileChooser'].filename+'\main.c' 
            codeDir.clear_code
            codeDir.clear_config
            codeDir.clear_sim
            codeDir.clear_dbg
            codeDir.do_write_note(mods){ |msg| bufferAddLnRolling 'createTextView',msg }
            codeDir.do_write(mods.configs){ |msg| bufferAddLnRolling 'createTextView',msg }
            bufferAddLnRolling 'createTextView',"完成工程生成"
            codeDir.do_write_userCode
            bufferAddLnRolling 'createTextView',"正在收尾"
            codeDir.close
            bufferAddLnRolling 'createTextView',"工程创建完成"
            bufferAddLnRolling 'createTextView'," "
            bufferAddLnRolling 'createTextView'," "
         else
            bufferAddLnRolling 'createTextView',"没有发现任何选择"
         end
      }
      self['doPretreatmentButton'].signal_connect("clicked"){ # 预处理 按钮
         if self['filechooserPret'].filename.class == String
            projectDir = ProjectDir.new self['filechooserPret'].filename
            bufferAddLnRolling 'pretreatmentMessageBox',"准备文件"
            projectDir.do_init
            case true
            when self['pretOutput'].active?
               bufferAddLnRolling 'pretreatmentMessageBox',"准备输出 单片机文件"
               bufferAddLnRolling 'pretreatmentMessageBox',"正在创建输出文件"
               projectDir.do_output
               bufferAddLnRolling 'pretreatmentMessageBox',"正在准备uv4工程"
               projectDir.create_uv4_project
            when self['pretTest'].active?
               bufferAddLnRolling 'pretreatmentMessageBox',"准备输出 仿真文件"
               bufferAddLnRolling 'pretreatmentMessageBox',"正在创建输出文件"
               projectDir.do_sim
            when self['pretDebug'].active?
               bufferAddLnRolling 'pretreatmentMessageBox',"准备输出 调试文件"
               projectDir.do_dbg
               bufferAddLnRolling 'pretreatmentMessageBox',"正在准备uv4工程"
               projectDir.create_uv4_project
            end
            projectDir.close
            self['outputProjectFileSelect'].filename=self['filechooserPret'].filename
            bufferAddLnRolling 'pretreatmentMessageBox',"完成输出"
            bufferAddLnRolling 'pretreatmentMessageBox',""
            bufferAddLnRolling 'pretreatmentMessageBox',""
            bufferAddLnRolling 'pretreatmentMessageBox',""
         else
            bufferAddLnRolling 'pretreatmentMessageBox',"没有发现任何选择"
         end
      }
      self['outputProjectFileSelect'].signal_connect("selection-changed"){
         self['outputToolCommand'].text = "-j0 -r #{self['outputProjectFileSelect'].filename}\\main.uvproj -o #{self['outputProjectFileSelect'].filename}\\log.txt"
         self['testToolCommand'].text = "#{self['outputProjectFileSelect'].filename}\\output.c -o #{self['outputProjectFileSelect'].filename}\\simu.exe"
      }
      self['outputButton'].signal_connect("clicked"){ # 输出 按钮
         bufferAddLnRolling 'outputMessage',"正在准备输出"
         toolUvString = ""
         if self['outputProjectFileSelect'].filename.class == String
            bufferAddLnRolling 'outputMessage',self['outputProjectFileSelect'].filename
            if self['outputToolPath'].filename.class == String
               bufferAddLnRolling 'outputMessage','获取UV4目录'
               
               configUV = self['outputToolPath'].filename
               $THREAD_FUN = true
               
               setF = File.new("setting","w")
               setF.write("@uv\n#{configUV}\n@gcc\n#{configGCC}\n")
               setF.close
               toolUvString = configUV+"\\uv4.exe "+self['outputToolCommand'].text
               bufferAddLnRolling 'outputMessage',toolUvString
               bufferAddLnRolling 'outputMessage',"Building ..."
               self['loadingWindow'].show_all
               while Gtk.events_pending?
                  Gtk.main_iteration
               end
               Thread.new{
                  `#{toolUvString}`
                  $THREAD_FUN = false
               }.join
               while($THREAD_FUN) do
                  while Gtk.events_pending?
                     Gtk.main_iteration
                  end
               end
               toolUvLog = File.new "#{self['outputProjectFileSelect'].filename}\\log.txt",'r'
               toolUvLog.each_line do |ln|
                  bufferAddLnRolling 'outputMessage',ln
               end
               toolUvLog.close

               while Gtk.events_pending?
                  Gtk.main_iteration
               end
               self['loadingWindow'].hide_all
               bufferAddLnRolling 'outputMessage','完成工程编译'
               bufferAddLnRolling 'outputMessage',' '
               bufferAddLnRolling 'outputMessage',' '
               bufferAddLnRolling 'outputMessage',' '
            else
               bufferAddLnRolling 'outputMessage','工具目录不存在'
            end
         else
            bufferAddLnRolling 'outputMessage','工程目录不存在'
         end
      }
      self['testButton'].signal_connect("clicked"){ # 测试 按钮
         bufferAddLnRolling 'outputMessage',"正在准备测试"
         toolGccString = ""
         if self['outputProjectFileSelect'].filename.class == String
            bufferAddLnRolling 'outputMessage',self['outputProjectFileSelect'].filename
            if self['gccPath'].filename.class == String
               bufferAddLnRolling 'outputMessage','获取GCC目录'
               configGCC = self['gccPath'].filename
               
               $THREAD_FUN = true
               setF = File.new("setting","w")
               setF.write("@uv\n#{configUV}\n@gcc\n#{configGCC}\n")
               setF.close
               toolGccString = configGCC+"\\gcc.exe "+self['testToolCommand'].text
               bufferAddLnRolling 'outputMessage',toolGccString
               self['loadingWindow'].show_all
               while Gtk.events_pending?
                  Gtk.main_iteration
               end
               Thread.new {
                  File.new(self['outputProjectFileSelect'].filename+"\\reg52.h","w").close
                  bufferAddLnRolling 'outputMessage',`#{toolGccString} 2>&1`
                  File.delete self['outputProjectFileSelect'].filename+"\\reg52.h"
                  bufferAddLnRolling 'outputMessage','--- 完成编译 ---'.center(20)
                  bufferAddLnRolling 'outputMessage','....'
                  bufferAddLnRolling 'outputMessage','--- 开始仿真 ---'.center(20)
                  bufferAddLnRolling 'outputMessage',' '
                  bufferAddLnRolling 'outputMessage',`#{self['outputProjectFileSelect'].filename}\\simu.exe` if File.exist? "#{self['outputProjectFileSelect'].filename}\\simu.exe"
                  #File.delete self['outputProjectFileSelect'].filename+"\\simu.exe"
                  $THREAD_FUN = false
               }.join
               while($THREAD_FUN) do
                  while Gtk.events_pending?
                     Gtk.main_iteration
                  end
               end
               self['loadingWindow'].hide_all
               bufferAddLnRolling 'outputMessage','完成仿真输出'
               bufferAddLnRolling 'outputMessage','完成仿真'
            else
               bufferAddLnRolling 'outputMessage','工具目录不存在'
            end
         else
            bufferAddLnRolling 'outputMessage','工程目录不存在'
         end
         
      }

      self['mainWindow'].show_all
   end

   def add_banner
      bannerFont = Pango::FontDescription.new "MS UI Gothic 8" 
      self['banner'].override_font bannerFont
      self['bannerBuffer'].set_text $versionString
   end

   #TreeViews
   def add_view_line(nameTreeView,packageName)
      iter = self[nameTreeView].model.append
      iter[0] = false
      iter[1] = packageName[0]
      iter[2] = packageName[1]
   end
   def add_all_ModTreeView(*msgString)
      add_view_line 'createTreeView',msgString
   end
   def get_select(nameTreeView)
      selected = []
      self[nameTreeView].model.each do |model,path,iter|
         selected.push [iter[1],iter[2]] if iter[0]
      end
      selected
   end


   def columns(nameTreeView)
      renderer = Gtk::CellRendererToggle.new
      renderer.signal_connect('toggled') do |cell,path|
         toggled(self[nameTreeView].model,path)
      end
      column = Gtk::TreeViewColumn.new("选择",renderer,'active' => 0)
      column.fixed_width = 80
      self[nameTreeView].append_column column

      column = Gtk::TreeViewColumn.new("名称",Gtk::CellRendererText.new,'text' => 1)
      column.fixed_width = 300
      column.set_sort_column_id(1)
      self[nameTreeView].append_column column

      column = Gtk::TreeViewColumn.new("路径",Gtk::CellRendererText.new,'text' => 2)
      column.set_sort_column_id(1)
      self[nameTreeView].append_column column
   end
   def toggled(model,path_str)
      path = Gtk::TreePath.new(path_str)

      iter = model.get_iter(path)
      selectBox = iter[0]
      selectBox ^= 1
      iter[0] = selectBox
   end

   #bufferOut
   def bufferChange(viewName,bufferString) #改变缓冲区内容
      self[viewName].buffer.set_text bufferString
   end
   def bufferAdd(viewName,bufferString) #缓冲区末尾加入
      self[viewName].buffer.insert self[viewName].buffer.end_iter,bufferString
   end
   def bufferAddLn(viewName,bufferString) #缓冲区末尾加入一行
      self[viewName].buffer.insert self[viewName].buffer.end_iter,bufferString+"\n"
   end
   def bufferRolling(viewName) #缓冲区滚动到末尾
      self[viewName].scroll_to_iter self[viewName].buffer.end_iter,0.4,true,0,1
   end
   def bufferAddLnRolling(viewName,bufferString)
      bufferAddLn viewName,bufferString
      bufferRolling viewName
      while Gtk.events_pending?
         Gtk.main_iteration
      end
   end

end

class Test
   @@tList = []

   @@tList.push 'test_dir'
	def test_dir
		t = CodeDir.new 'd:\work\Bishe\Test\main.c'
      
      testlist = [
         ["@codeFile.path",'.casecmp','d:\work\Bishe\Test\main.c',"codeFile"],
         ["@modDir.path",'.casecmp',Dir.pwd + '\mods',"modDir"],
         ["self.path",'.casecmp','d:\work\Bishe\Test',"scriptDir"],
         ["@codeMod.path",'==','nil',"codeMod is NULL"]
      ]
      t.instance_eval do
         
         testlist.each do |line|
            eval("puts \"#{line[3]} ==> \"+#{line[0]} unless #{line[0]}#{line[1]} \"#{line[2]}\"")
         end
      end
      
      puts t.modList

	end


	def initialize
		@@tList.each do |temp|
			self.send temp.to_sym
		end
	end
end

builder = GuiBuilder.new 'mainWindow.glade'
#Test.new


Gtk.main

